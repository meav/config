if #arg < 1 then
    print("Error")
    os.exit(0)
end
local program = arg[1]
local password = "abc"
if program == "ssh" then
    sshpid = spawn("ssh","-p","8022","192.168.1.3","-t","tmux","new","-A","-s","dat")
elseif program == "sftp" then
    sshpid = spawn("sftp","-P","8022","192.168.1.3")
else
end
if not sshpid then
    print("ssh or sftp is not found")
    os.exit(1)
end
timeout = 10
capturelines = 3 -- default is 2

while true do
    local rc = expect(
    "password:",
    "Are you sure you want to continue connecting (yes/no/[fingerprint])?",
    "Could not resolve hostname")

    if rc == 0 then
        sendln(password)
        sleep(1)
        rc = expect("Permission denied, please try again")
        if rc == 0 then
            print()
            print("Wrong password")

            kill(sshpid)
        else
            sleep(1)
            wait(sshpid)
            sleep(1)
            print()
            print("Done")
        end
        break
    elseif rc == 1 then
        sendln("yes")
        print() -- move cursor down not to capture same keyword
    elseif rc == -2 then
        print("TIMEOUT")
        break
    elseif rc == -1 then
        print("ERROR")
        break
    else
        echo(string.format("Error keyword found \"%s\". Exit",_MATCH))
        break
    end
end
