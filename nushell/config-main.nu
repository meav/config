$env.EDITOR = 'hx'
$env.PAGER = 'moar'
$env.MOAR = '-colors 256 -no-statusbar -style pygments -wrap'

$env.config.table.mode = 'thin'
$env.config.table.index_mode = 'never'
$env.config.table.show_empty = true
$env.config.table.header_on_separator = true

use ~/.config/nushell/papercolor.nu
$env.config.color_config = (papercolor)

$env.config.buffer_editor = 'hx'

$env.config.cursor_shape.emacs = 'line'

use '/data/data/com.termux/files/home/.config/broot/launcher/nushell/br' *

use ~/.cache/starship/init.nu

source ~/.config/nushell/just.nu
let fish_completer = {|spans|
    fish --command $'complete "--do-complete=($spans | str join " ")"'
    | from tsv --flexible --noheaders --no-infer
    | rename value description
}
$env.config.completions.external.enable = true
$env.config.completions.external.completer = $fish_completer

$env.config.keybindings ++= [
  {
    name: ide_completion_menu
    modifier: none
    keycode: tab
    mode: [emacs vi_normal vi_insert]
    event: {
      until: [
        { send: menu name: ide_completion_menu }
        { send: menunext }
        { edit: complete }
      ]
    }
  }
  {
    name: completion_menu
    modifier: control
    keycode: space
    mode: [emacs vi_normal vi_insert]
    event: {
      until: [
        { send: menu name: completion_menu }
        { send: menunext }
        { edit: complete }
      ]
    }
  }
  {
    name: open_command_editor
    modifier: control
    keycode: char_o
    mode: [emacs, vi_normal, vi_insert]
    event: { send: openeditor }
  }
  {
    name: move_to_line_end_or_take_history_hint
    modifier: control
    keycode: char_l
    mode: [emacs, vi_normal, vi_insert]
    event: {
      until: [
        { send: historyhintcomplete }
        { edit: movetolineend }
      ]
    }
  }
  {
    name: move_to_line_end_or_take_history_hint
    modifier: control
    keycode: right
    mode: [emacs, vi_normal, vi_insert]
    event: {
      until: [
        { send: historyhintcomplete }
        { edit: movetolineend }
      ]
    }
  }
  {
    name: move_right_or_take_one_word_history_hint
    modifier: none
    keycode: right
    mode: [emacs, vi_normal, vi_insert]
    event: {
      until: [
        { send: historyhintwordcomplete }
        { send: menuright }
        { send: right }
      ]
    }
  }
  {
    name: move_up
    modifier: control
    keycode: char_k
    mode: [emacs, vi_normal, vi_insert]
    event: {
      until: [
        { send: menuup }
        { send: up }
      ]
    }
  }
  {
    name: move_down
    modifier: control
    keycode: char_j
    mode: [emacs, vi_normal, vi_insert]
    event: {
      until: [
        { send: menudown }
        { send: down }
      ]
    }
  }
  {
    name: fuzzy_history
    modifier: control
    keycode: char_h
    mode: [emacs, vi_normal, vi_insert]
    event: [
      {
        send: ExecuteHostCommand
        cmd: "do {
          commandline edit --insert (
            history
            | get command
            | reverse
            | uniq
            | str join (char -i 0)
            | fzf --scheme=history 
              --read0
              --no-sort
              --no-multi
              --extended
              --exact
              --preview-window up:6,wrap
              --ansi
              --bind=esc:abort,tab:up
              --preview='echo {} | nu --config /data/data/com.termux/files/home/.config/nushell/config.nu --stdin -c nu-highlight'
              --query (commandline)
            | decode utf-8
            | str trim
          )
        }"
      }
    ]
  }
]

alias core-ls = ls
alias ls = core-ls -a
alias mpvprefix = commandline edit --insert ' mpv.com --title='
alias his = commandline edit --insert (
  history
  | get command
  | reverse
  | uniq
  | str join (char -i 0)
  | fzf --scheme=history 
    --read0
    --no-sort
    --no-multi
    --extended
    --exact
    --preview-window up:6,wrap
    --ansi
    --bind=esc:abort,tab:up
    --preview='echo {} | nu --config /data/data/com.termux/files/home/.config/nushell/config.nu --stdin -c nu-highlight'
    --query (commandline)
  | decode utf-8
  | str trim
)

if ('dls' | path exists) {
    cd dls
    br
} else {
    br
}
