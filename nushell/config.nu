$env.config.table.mode = 'thin'
$env.config.table.index_mode = 'never'
$env.config.table.show_empty = true
$env.config.table.header_on_separator = true
use ~/.config/nushell/papercolor.nu
$env.config.color_config = (papercolor)
