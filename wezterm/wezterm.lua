-- https://github.com/wez/wezterm/issues/2800
-- https://github.com/wez/wezterm/issues/1485#issuecomment-1689047779
-- start "" wezterm-gui start -- broot %HOMEDRIVE%%HOMEPATH%/Desktop
-- start "" wezterm-gui ssh 192.168.1.4:8022
-- Pull in the wezterm API
local wezterm = require 'wezterm'
local act = wezterm.action

-- This table will hold the configuration.
local config = {}

-- In newer versions of wezterm, use the config_builder which will
-- help provide clearer error messages
if wezterm.config_builder then
  config = wezterm.config_builder()
end

-- This is where you actually apply your config choices
config.front_end = 'OpenGL'
-- Disable check for update
config.check_for_updates = false
-- For example, changing the color scheme:
local percolo = wezterm.color.get_builtin_schemes()['OneHalfLight']
percolo.background = 'rgb(255,255,255)'
percolo.foreground = 'rgb(0,0,0)'
percolo.cursor_bg = '#516d7b'
percolo.cursor_border = '#516d7b'
percolo.cursor_fg = '#ebf8ff'
config.color_schemes = {
  ['OneHalfLight + AtelierLakeside'] = percolo,
}
config.color_scheme = 'OneHalfLight + AtelierLakeside'
-- Font
-- This tells wezterm to look first for fonts in the directory named
-- `fonts` that is found alongside your `wezterm.lua` file.
-- As this option is an array, you may list multiple locations if
-- you wish.
config.font_dirs = { 'fonts' }
-- Font
config.font = wezterm.font_with_fallback {
  'VictorMono NF Medium',
}
-- Font rules
config.font_rules = {
  {
    intensity = 'Bold',
    italic = false,
    font = wezterm.font {
      family = 'VictorMono NF',
      weight = 'Bold',
    },
  },
  {
    intensity = 'Bold',
    italic = true,
    font = wezterm.font {
      family = 'VictorMono NF',
      weight = 'Bold',
      style = 'Italic',
    },
  },
  {
    italic = true,
    intensity = 'Normal',
    font = wezterm.font {
      family = 'VictorMono NF Medium',
      weight = 'Medium',
      style = 'Italic',
    },
  },
}
-- Font size
config.font_size = 20
-- Line height
config.line_height = 1.1
-- Window padding
config.window_padding = {
  left = 0,
  right = "2%",
  top = 0,
  bottom = 0,
}
-- Auto maximize
config.initial_cols = 200
config.initial_rows = 50
-- Cursor
config.default_cursor_style = 'SteadyBar'
config.underline_thickness = '250%'
-- Disable title bar
-- config.window_decorations = "INTEGRATED_BUTTONS|RESIZE"
config.window_decorations = "RESIZE"
-- Enable tabbar
config.enable_tab_bar = true
-- Enable retro tabbar style
config.use_fancy_tab_bar = false
-- Max tab max width
config.tab_max_width = 200
-- Hide new tab button
config.show_new_tab_button_in_tab_bar = false
-- Display of minimize, maximize, close button in tabbar
-- local window_min = ' 󰖰 '
-- local window_max = ' 󰖯 '
-- local window_close = ' 󰅖 '
-- config.tab_bar_style = {
--     window_hide = window_min,
--     window_hide_hover = window_min,
--     window_maximize = window_max,
--     window_maximize_hover = window_max,
--     window_close = window_close,
--     window_close_hover = window_close,
-- }
-- Add clock to left tabbar
wezterm.on('update-right-status', function(window, pane)
  local time = wezterm.strftime '%H:%M:%S'
  window:set_left_status(wezterm.format {
    {Attribute={Intensity="Bold"}},
    {Foreground={Color="#b7eb34"}},
    {Background={Color="#eb4034"}},
    { Text = ' ' .. time .. ' '},
  })
end)
-- Equivalent to POSIX basename(3)
-- Given "/foo/bar" returns "bar"
-- Given "c:\\foo\\bar" returns "bar"
function basename(s)
  return string.gsub(s, '(.*[/\\])(.*)', '%2')
end
-- This function returns the suggested title for a tab.
-- It prefers the title that was set via `tab:set_title()`
-- or `wezterm cli set-tab-title`, but falls back to the
-- title of the active pane in that tab.
function tab_title(tab_info)
  local title = tab_info.tab_title
  local index = tab_info.tab_index + 1
  -- if the tab title is explicitly set, take that
  if title and #title > 0 then
    return index .. ' ' .. title .. ' '
  end
  -- Otherwise, use the title from the active pane
  -- in that tab
  local pane = tab_info.active_pane
  local title = basename(pane.foreground_process_name)
  return index .. ' ' .. title .. ' '
end
-- Tab bar devide equal for each tab and center tab name
wezterm.on('format-tab-title', function(tab, tabs, panes, config, hover, max_width)
  -- Not sure if it will slow down the performance, at least so far it's good
  -- Is there a better way to get the tab or window cols ?
  local mux_window = wezterm.mux.get_window(tab.window_id)
  local mux_tab = mux_window:active_tab()
  local mux_tab_cols = (mux_tab:get_size().cols - 10)

  -- Calculate active/inactive tab cols
  -- In general, active tab cols > inactive tab cols
  local tab_count = #tabs
  local inactive_tab_cols = math.floor(mux_tab_cols / tab_count)
  local active_tab_cols = inactive_tab_cols

  local title = tab_title(tab)
  title = ' ' .. title .. ' '
  local title_cols = wezterm.column_width(title) + 2

  -- Divide into 3 areas and center the title
  if tab.is_active then
    local rest_cols = math.max(active_tab_cols - title_cols, 0)
    local right_cols = math.ceil(rest_cols / 2)
    local left_cols = right_cols
    return {
      -- left
      { Text = wezterm.pad_right('', left_cols) },
      -- center
      { Text = ' ' .. title },
      -- right
      { Text = wezterm.pad_right('', right_cols) },
    }
  else
    local rest_cols = math.max(inactive_tab_cols - title_cols, 0)
    local right_cols = math.ceil(rest_cols / 2)
    local left_cols = right_cols
    return {
      -- left
      { Text = wezterm.pad_right('', left_cols) },
      -- center
      { Attribute = { Italic = true } },
      { Text = ' ' .. title },
      -- right
      { Text = wezterm.pad_right('', right_cols) },
    }
  end
end)
-- Tabbar color
local tabbarnothover = {
      bg_color = '#e6fce6',
      fg_color = '#000000',
    }
local tabbarhover = {
      bg_color = '#ffffff',
      fg_color = '#439972',
      -- Specify whether you want "Half", "Normal" or "Bold" intensity for the
      -- label shown for this tab.
      -- The default is "Normal"
      intensity = 'Bold',
    }
config.colors = {
  tab_bar = {
    -- The color of the strip that goes along the top of the window
    -- (does not apply when fancy tab bar is in use)
    background = '#ffffff',

    -- The active tab is the one that has focus in the window
    active_tab = {
      -- The color of the background area for the tab
      bg_color = '#ffffff',
      -- The color of the text for the tab
      fg_color = '#d70087',

      -- Specify whether you want "Half", "Normal" or "Bold" intensity for the
      -- label shown for this tab.
      -- The default is "Normal"
      intensity = 'Normal',

      -- Specify whether you want the text to be italic (true) or not (false)
      -- for this tab.  The default is false.
      italic = false,

      -- Specify whether you want the text to be rendered with strikethrough (true)
      -- or not for this tab.  The default is false.
      strikethrough = false,
    },

    -- Inactive tabs are the tabs that do not have focus
    inactive_tab = tabbarnothover,

    -- You can configure some alternate styling when the mouse pointer
    -- moves over inactive tabs
    inactive_tab_hover = tabbarhover,

    -- The new tab button that let you create new tabs
    new_tab = tabbarnothover,

    -- You can configure some alternate styling when the mouse pointer
    -- moves over the new tab button
    new_tab_hover = tabbarhover,

  },
}
-- Command palette style
config.command_palette_font_size = 18.0
config.command_palette_bg_color = "#ffffff"
config.command_palette_fg_color = "#105222"
-- Open broot in newtab
local brootnewtab = act.SpawnCommandInNewTab {
  args = { 'broot.exe' },
  cwd = '%HOMEDRIVE%%HOMEPATH%',
}
-- Command palette custom command
wezterm.on('augment-command-palette', function(window, pane)
  return {
    {
      brief = 'New tab broot.exe',
      action = brootnewtab,
    },
    {
      brief = 'New tab cmd.exe',

      action = act.Multiple {
        -- Create new tab
        act.SpawnCommandInNewTab {
          args = { 'cmd.exe' },
          set_environment_variables = {
            -- This changes the default prompt for cmd.exe to report the
            -- current directory using OSC 7, show the current directory colored in the prompt.
            prompt = '$E]7;file://localhost/$P$E\\$E[0m$E[35m$P$G$E[0m ',
          },
        },
        -- change tab title
        wezterm.action_callback(function(window, pane)
            local index = window:active_tab().tab_index + 1
            window:active_tab():set_title(index .. ' cmd ')
        end),
      },
    },
    {
      brief = 'Rename tab',

      action = act.PromptInputLine {
        description = 'Enter new name for tab',
        action = wezterm.action_callback(function(window, pane, line)
          if line then
            window:active_tab():set_title(line)
          end
        end),
      },
    },
  }
end)
-- Only show tab bar when have more than 1 tab
-- config.hide_tab_bar_if_only_one_tab = true
-- Auto maximize when start
-- local mux = wezterm.mux
-- wezterm.on('gui-startup', function(cmd)
--   local tab, pane, window = mux.spawn_window(cmd or {})
--   window:gui_window():maximize()
-- end)
-- Key bindings
config.keys = {
  -- Turn off the default CMD-m Hide action, allowing CMD-m to
  -- be potentially recognized and handled by the tab
  -- {
  --   key = 'm',
  --   mods = 'CMD',
  --   action = act.DisableDefaultAssignment,
  -- },
  {
    key = 'LeftArrow',
    mods = 'SHIFT|CTRL',
    action = act.ActivateTabRelative(-1),
  },
  {
    key = 'h',
    mods = 'SHIFT|CTRL',
    action = act.ActivateTabRelative(-1),
  },
  {
    key = 'RightArrow',
    mods = 'SHIFT|CTRL',
    action = act.ActivateTabRelative(1),
  },
  {
    key = 'l',
    mods = 'SHIFT|CTRL',
    action = act.ActivateTabRelative(1),
  },
  {
    key = 'UpArrow',
    mods = 'SHIFT|CTRL',
    action = brootnewtab,
  },
  {
    key = 'k',
    mods = 'SHIFT|CTRL',
    action = brootnewtab,
  },
  -- Show Command Palette
  {
    key = 'DownArrow',
    mods = 'SHIFT|CTRL',
    action = act.ActivateCommandPalette,
  },
  {
    key = 'j',
    mods = 'SHIFT|CTRL',
    action = act.ActivateCommandPalette,
  },
}
-- Mouse bindings
config.mouse_bindings = {
  -- Change the default click behavior so that it only selects
  -- text and doesn't open hyperlinks
  {
    event = { Up = { streak = 1, button = 'Left' } },
    mods = 'NONE',
    action = act.CompleteSelection 'ClipboardAndPrimarySelection',
  },
  -- and make CTRL-SHIFT-Click up open hyperlinks, disable CTRL-Click down
  {
    event = { Up = { streak = 1, button = 'Left' } },
    mods = 'CTRL|SHIFT',
    action = act.OpenLinkAtMouseCursor,
  },
  {
    event = { Down = { streak = 1, button = 'Left' } },
    mods = 'CTRL|SHIFT',
    action = act.Nop,
  },
-- Paste from clipboard when Right up, disable Right down
  {
    event = { Up = { streak = 1, button = 'Right' } },
    mods = 'NONE',
    action = act.PasteFrom 'Clipboard',
  },
  {
    event = { Down = { streak = 1, button = 'Right' } },
    mods = 'NONE',
    action = act.Nop,
  },
}
-- Disable all default key bindings
-- config.disable_default_key_bindings = true
-- and finally, return the configuration to wezterm
return config
