let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

let g:plug_window = 'tab new'

call plug#begin(stdpath('data') . '/plugged')
Plug 'C:\Users\Administrator\Music\Codeberg\config\nvim\plugins\lightline'
Plug 'C:\Users\Administrator\Music\Codeberg\config\nvim\plugins\FocusPreviousWindowsWhenClose'
Plug 'wimstefan/vim-artesanal'
Plug 'tpope/vim-surround'
Plug 'https://codeberg.org/timoses/vim-venu.git'
Plug 'leafOfTree/vim-matchtag'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'sheerun/vim-polyglot'
Plug 'equalsraf/neovim-gui-shim'
Plug 'tpope/vim-commentary'
Plug 'mbbill/undotree'
Plug 'junegunn/vim-peekaboo'
Plug 'ryanoasis/vim-devicons'
call plug#end()

" Color
set termguicolors
set background=light
colorscheme artesanal

" Disable shada
set shada="NONE"

" Cursor
set cursorline
set guicursor=n-v-ve:hor20-Cursorgreen/lCursorgreen,i-c-ci:ver25-Cursorgreen/lCursorgreen,r-cr:hor20-blinkwait700-blinkon1000-blinkoff700-Cursorgreen/lCursorgreen,o:hor50-Cursorred/lCursorred

" Encoding
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
set termencoding=utf-8
set encoding=utf-8

" Line number netrw
let g:netrw_bufsettings = 'noma nomod nu nowrap ro nobl'
" Open Netrw after Vim starts up
augroup InitNetrw
  autocmd!
  autocmd VimEnter * :silent! Explore
augroup END

" Enable highlight syntax
syntax enable

" Column number
set number
set relativenumber
set numberwidth=4

" Scroll before bottom
set scrolloff=7

" Lightline
set noshowmode
set laststatus=2
set showtabline=2
"""
let g:lightline = {
      \ 'colorscheme': 'ayu_mirage',
      \ 'active': {
      \   'left': [ [ 'filename' ],
      \             [ 'readonly', 'modified', 'split' ] ],
      \   'right': [ [ 'fileencoding' ],
      \             [ 'fileformat' ],
      \             [ 'percent' ],
      \             [ 'lineinfo' ],
      \             [ 'mode' ] ]
      \ },
      \ 'inactive': {
      \   'left': [ [ 'filename' ],
      \             [ 'readonly', 'modified', 'split' ] ],
      \   'right': [ [ 'fileencoding' ],
      \             [ 'fileformat' ],
      \             [ 'percent' ],
      \             [ 'lineinfo' ],
      \             [ 'mode' ] ]
      \ },
      \   'tabline': {
      \     'left': [ ['tabs'],
      \               ['timer'] ],
      \     'right': [ ['date'],
      \                ['ampm'],
      \                ['clock'] ]
      \   },
      \ 'component': {
      \   'filename': '%F',
      \   'lineinfo': '%l/%L',
      \   'split': '-',
      \ },
      \ 'component_function': {
      \   'readonly': 'LightlineReadonly',
      \   'clock': 'LightlineClock',
      \   'ampm': 'LightlineAmpm',
      \   'date': 'LightlineDate',
      \   'timer': 'LightlineTimer',
      \   'fileformat': 'MyFileformat',
      \ },
      \ }
"""
function! MyFileformat()
  return &fileformat . ' ' . WebDevIconsGetFileFormatSymbol()
endfunction
"""
function! LightlineReadonly()
  return &readonly ? '[Read-only]' : ''
endfunction
""""
function! LightlineClock()
  return strftime('%I:%M')
endfunction
"""
function! LightlineAmpm()
  return strftime('%p')
endfunction
"""
function! LightlineDate()
  return strftime('%A, %d/%m/%Y')
endfunction
"""
function! LightlineTimer()
  let s:lines = readfile('C:\Users\Administrator\Music\cygwin\cygwin\home\meav\.timer')
  for s:line in s:lines
    return s:line
  endfor
endfunction

" Mapping
map <C-a> <esc>:%y*<CR>
map <C-h> <esc><C-w>h
map <C-j> <esc><C-w>j
map <C-k> <esc><C-w>k
map <C-l> <esc><C-w>l
tnoremap <C-Space> <C-\><C-n>
augroup NetrwGroup
  autocmd!
  autocmd filetype netrw call NetrwMapping()
augroup END
function! NetrwMapping()
  nmap <buffer> o <CR>
endfunction
command Neoviminit edit ~/Appdata/Local/nvim/init.vim
command Neovimqt !explorer "C:\Users\Administrator\Music\nvim-qt.exe.lnk"
command Cent !explorer "C:\Users\Administrator\Music\centbrowser_3.4.3.39_x64_portable\chrome.exe"
command Facebook !explorer "C:\Users\Administrator\Music\centbrowser_3.4.3.39_x64_portable\Facebook.lnk"
command Timer !explorer "C:\Users\Administrator\Music\cygwin\timer.cmd"
command G Gtabedit :
function! Power()
  !explorer "D:\Users\a\Music\WW\power.vbs"
  quitall!
endfunction
command Power call Power()
command Desktop !explorer "D:\Users\a\Music\WW\desktop.vbs"

" Custom theme
highlight Normal guibg=#ffffff
highlight CursorLine guifg='NONE' guibg=#ffffff gui='NONE'
highlight CursorLineNr guibg=#e6fce6 guifg=#5f5f00 cterm=bold gui=bold
highlight LineNr guibg=#e6fce6 guifg=#383a42
highlight Comment cterm=italic gui=italic
highlight MatchParen cterm=bold gui=bold
highlight Function cterm=bold gui=bold
highlight Statement cterm=bold gui=bold
highlight Conditional cterm=bold gui=bold
highlight Repeat cterm=bold gui=bold
highlight Label cterm=bold gui=bold
highlight PreProc cterm=bold gui=bold
highlight Include cterm=bold gui=bold
highlight Define cterm=bold gui=bold
highlight Macro cterm=bold gui=bold
highlight PreCondit cterm=bold gui=bold
highlight Type cterm=bold gui=bold
highlight StorageClass cterm=bold gui=bold
highlight Todo cterm=bold gui=bold
highlight Cursorgreen guifg=green guibg=green
highlight Cursorred guifg=red guibg=red
" Matchtag theme
highlight matchTag guibg=#0a801c guifg=#ffffff cterm=bold gui=bold
highlight link matchTagError Error

" Venu
let s:menu = venu#create('Venu')
"""
let s:help = venu#create('Help')
call venu#addItem(s:help, 'Shortcut when use :help', 'echo "Follow link in :help\nCtrl+]\nBack link in :help\nCtrl+T"')
call venu#addItem(s:help, 'Shortcut netrw', 'echo "Rename: R\nChange working directory: cd"')
call venu#addItem(s:help, 'Commentary', 'tab help gc')
call venu#addItem(s:help, 'Undotree', 'tab help undotree-usage')
call venu#addItem(s:help, 'Surround', 'tab help Surround')
call venu#addItem(s:help, 'Color of item', 'echo "Color of Comment\n:verbose hi Comment"')
call venu#addItem(s:menu, 'Help', s:help)
"""
let s:snippet = venu#create('Snippets')
call venu#addItem(s:snippet, 'Snippets', 'exe "normal ivoz"|exe "normal ovn-z<>"')
call venu#addItem(s:menu, 'Snippets', s:snippet)
"""
call venu#addItem(s:menu, 'List Plugins', 'PlugStatus')
"""
call venu#register(s:menu)

" Auto reload tabline
augroup reloadtabline
  au!
  au BufEnter,InsertEnter,CursorMoved,FocusGained * redrawtabline
augroup END

" Undotree setting
let g:undotree_WindowLayout = 2
let g:undotree_SetFocusWhenToggle = 1
let g:undotree_ShortIndicators = 1
function g:Undotree_CustomMap()
    nmap <buffer> k <plug>UndotreeNextState
    nmap <buffer> j <plug>UndotreePreviousState
endfunc

" Peekaboo floating window
function! CreateCenteredFloatingWindow()
    let width = float2nr(&columns * 0.6)
    let height = float2nr(&lines * 0.6)
    let top = ((&lines - height) / 2) - 1
    let left = (&columns - width) / 2
    let opts = {'relative': 'editor', 'row': top, 'col': left, 'width': width, 'height': height, 'style': 'minimal'}

    let top = "╭" . repeat("─", width - 2) . "╮"
    let mid = "│" . repeat(" ", width - 2) . "│"
    let bot = "╰" . repeat("─", width - 2) . "╯"
    let lines = [top] + repeat([mid], height - 2) + [bot]
    let s:buf = nvim_create_buf(v:false, v:true)
    call nvim_buf_set_lines(s:buf, 0, -1, v:true, lines)
    call nvim_open_win(s:buf, v:true, opts)
    set winhl=Normal:Floating
    let opts.row += 1
    let opts.height -= 2
    let opts.col += 2
    let opts.width -= 4
    call nvim_open_win(nvim_create_buf(v:false, v:true), v:true, opts)
    au BufWipeout <buffer> exe 'bw '.s:buf
endfunction
let g:peekaboo_window="call CreateCenteredFloatingWindow()"
