local user_packadd_path = "faerryn_user.nvim/default/default/default/default"
local user_install_path = vim.fn.stdpath "data" .. "/site/pack/user/opt/" .. user_packadd_path
if vim.fn.isdirectory(user_install_path) == 0 then
    os.execute(
        "git clone --quiet --depth 1 https://github.com/faerryn/user.nvim.git " .. vim.fn.shellescape(user_install_path)
    )
end
vim.api.nvim_command("packadd " .. vim.fn.fnameescape(user_packadd_path))
-- Setup and plugins
local user = require("user")
user.setup()
local use = user.use
-- user.nvim can manage itself!
use "faerryn/user.nvim"
use({
	"nvim-treesitter/nvim-treesitter",
	config = function ()
		use("p00f/nvim-ts-rainbow")
		use("nvim-treesitter/playground")
		require("treesitter-settings")
	end
})
use({
	"maxmx03/solarized.nvim",
	config = function ()
		require("colorscheme-settings")
	end
})
use({
	"nvim-telescope/telescope.nvim",
	init = function ()
		use("nvim-lua/plenary.nvim")
		use("kyazdani42/nvim-web-devicons")
	end,
	config = function ()
		require("telescope-settings")
	end
})
use({
	"AckslD/nvim-neoclip.lua",
	init = function ()
		use("nvim-telescope/telescope.nvim")
	end,
	config = function ()
		require("neoclip-settings")
	end
})
use({
	"sheerun/vim-polyglot",
	config = function ()
		require("polyglot-settings")
	end
})
use({
	"nanozuki/tabby.nvim",
	init = function ()
		use("kyazdani42/nvim-web-devicons")
	end,
	config = function ()
		require("tabby-settings")
	end
})
use({
	"nvim-lualine/lualine.nvim",
	init = function ()
		use("kyazdani42/nvim-web-devicons")
	end,
	config = function ()
		require("lualine-settings")
	end
})
use({
	"folke/noice.nvim",
	init = function ()
		use("MunifTanjim/nui.nvim")
		use("rcarriga/nvim-notify")
	end,
	config = function ()
		require("noice-settings")
	end
})
use("echasnovski/mini.surround")
use("echasnovski/mini.pairs")
use("echasnovski/mini.indentscope")
use({
	"echasnovski/mini.comment",
	config = function ()
		require("mini-nvim-settings")
	end
})
use({
	"sudormrfbin/cheatsheet.nvim",
	init = function ()
		use("nvim-telescope/telescope.nvim")
	end,
	config = function ()
		require("cheatsheet-settings")
	end
})
