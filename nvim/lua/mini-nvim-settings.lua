require('mini.surround').setup({
  -- Duration (in ms) of highlight when calling `MiniSurround.highlight()`
  highlight_duration = 5000,
})

require('mini.pairs').setup()

require('mini.comment').setup()

require('mini.indentscope').setup()
