local M = {}

local icon_colors = {
  Brown        = '#905532',
  Aqua         = '#3AFFDB',
  Blue         = '#689FB6',
  Darkblue     = '#44788E',
  Purple       = '#834F79',
  Red          = '#AE403F',
  Beige        = '#F5C06F',
  Yellow       = '#F09F17',
  Orange       = '#D4843E',
  Darkorange   = '#F16529',
  Pink         = '#CB6F6F',
  Salmon       = '#EE6E73',
  Green        = '#8FAA54',
  Lightgreen   = '#31B53E',
  White        = '#FFFFFF',
  LightBlue    = '#5fd7ff',
}

local icons = {
  Brown        = {''},
  Aqua         = {''},
  LightBlue    = {'','',''},
  Blue         = {'','','','','','','','','','','','',''},
  Darkblue     = {'',''},
  Purple       = {'','','','',''},
  Red          = {'','','','','',''},
  Beige        = {'','',''},
  Yellow       = {'','','λ','',''},
  Orange       = {'',''},
  Darkorange   = {'','','','',''},
  Pink         = {'',''},
  Salmon       = {''},
  Green        = {'','','','','',''},
  Lightgreen   = {'','','﵂'},
  White        = {'','','','','',''},
}

-- filetype or extensions : { colors ,icon}
local user_icons = {}

function M.define_file_icon()
  return  user_icons
end

local util = require('tabby.util')

local components = function()
  local coms = {}
  local tabs = vim.api.nvim_list_tabpages()
  local current_tab = vim.api.nvim_get_current_tabpage()
  for _, tabid in ipairs(tabs) do
    if tabid == current_tab then
      table.insert(coms, {
        type = 'tab',
        tabid = tabid,
        label = {
          '  ' .. vim.api.nvim_tabpage_get_number(tabid) .. ' ' ,
          hl = { fg = '#d70087', bg = '#fdf6e3', style = 'bold' },
        },
      })
      local wins = util.tabpage_list_wins(current_tab)
      local top_win = vim.api.nvim_tabpage_get_win(current_tab)
      for _, winid in ipairs(wins) do
        local icon = ' '
        if winid == top_win then
          icon = ' '
        end
        local bufid = vim.api.nvim_win_get_buf(winid)
        local buf_name = vim.api.nvim_buf_get_name(bufid)
        table.insert(coms, {
          type = 'win',
          winid = winid,
          label = {
            icon .. vim.fn.fnamemodify(buf_name, ':t') .. ' ',
            hl = { fg = '#d70087', bg = '#fdf6e3' },
          },
        })

        local function get_file_info()
          return vim.fn.fnamemodify(buf_name, ':t'),vim.fn.fnamemodify(buf_name, ':e')
        end
        function M.get_file_icon()
          local fileicon = ''
          if vim.fn.exists("*WebDevIconsGetFileTypeSymbol") == 1 then
            fileicon = vim.fn.WebDevIconsGetFileTypeSymbol()
            return fileicon .. ' '
          end
          local ok,devicons = pcall(require,'nvim-web-devicons')
          if not ok then print('No icon plugin found. Please install \'kyazdani42/nvim-web-devicons\'') return '' end
          local f_name,f_extension = get_file_info()
          fileicon = devicons.get_icon(f_name,f_extension)
          if fileicon == nil then
            if user_icons[vim.bo.filetype] ~= nil then
              fileicon = user_icons[vim.bo.filetype][2]
            elseif user_icons[f_extension] ~= nil then
              fileicon = user_icons[f_extension][2]
            else
              fileicon = ''
            end
          end
          return fileicon .. ' '
        end
        function M.get_file_icon_color()
          local filetype = vim.bo.filetype
          local f_name, f_ext = get_file_info()
          if user_icons[filetype] ~= nil then return user_icons[filetype][1] end
          if user_icons[f_ext] ~= nil then return user_icons[f_ext][1] end
          local has_devicons, devicons = pcall(require, 'nvim-web-devicons')
          if has_devicons then
            local fileicon, iconhl = devicons.get_icon(f_name, f_ext)
            if fileicon ~= nil then
              return vim.fn.synIDattr(vim.fn.hlID(iconhl), 'fg')
            end
          end
          local fileicon = M.get_file_icon():match('%S+')
          for k, _ in pairs(icons) do
            if vim.fn.index(icons[k], icon) ~= -1 then return icon_colors[k] end
          end
        end

        table.insert(coms, {
          type = 'text',
          text = {
            M.get_file_icon(),
            hl = { fg = M.get_file_icon_color(), bg = '#fdf6e3' },
          },
        })
      end
    else
      table.insert(coms, {
        type = 'tab',
        tabid = tabid,
        label = {
          '  ' .. vim.api.nvim_tabpage_get_number(tabid) .. ' ' ,
          hl = { fg = '#000000', bg = '#e6fce6', style = 'italic' },
        },
      })
      local wins = util.tabpage_list_wins(tabid)
      local top_win = vim.api.nvim_tabpage_get_win(tabid)
      for _, winid in ipairs(wins) do
        local icon = ' '
        if winid == top_win then
          icon = ' '
        end
        local bufid = vim.api.nvim_win_get_buf(winid)
        local buf_name = vim.api.nvim_buf_get_name(bufid)
        table.insert(coms, {
          type = 'win',
          winid = winid,
          label = {
            icon .. vim.fn.fnamemodify(buf_name, ':t') .. ' ',
            hl = { fg = '#000000', bg = '#e6fce6', style = 'italic' },
          },
        })

        local function get_file_info()
          return vim.fn.fnamemodify(buf_name, ':t'),vim.fn.fnamemodify(buf_name, ':e')
        end
        function M.get_file_icon()
          local fileicon = ''
          if vim.fn.exists("*WebDevIconsGetFileTypeSymbol") == 1 then
            fileicon = vim.fn.WebDevIconsGetFileTypeSymbol()
            return fileicon .. ' '
          end
          local ok,devicons = pcall(require,'nvim-web-devicons')
          if not ok then print('No icon plugin found. Please install \'kyazdani42/nvim-web-devicons\'') return '' end
          local f_name,f_extension = get_file_info()
          fileicon = devicons.get_icon(f_name,f_extension)
          if fileicon == nil then
            if user_icons[vim.bo.filetype] ~= nil then
              fileicon = user_icons[vim.bo.filetype][2]
            elseif user_icons[f_extension] ~= nil then
              fileicon = user_icons[f_extension][2]
            else
              fileicon = ''
            end
          end
          return fileicon .. ' '
        end
        function M.get_file_icon_color()
          local filetype = vim.bo.filetype
          local f_name, f_ext = get_file_info()
          if user_icons[filetype] ~= nil then return user_icons[filetype][1] end
          if user_icons[f_ext] ~= nil then return user_icons[f_ext][1] end
          local has_devicons, devicons = pcall(require, 'nvim-web-devicons')
          if has_devicons then
            local fileicon, iconhl = devicons.get_icon(f_name, f_ext)
            if fileicon ~= nil then
              return vim.fn.synIDattr(vim.fn.hlID(iconhl), 'fg')
            end
          end
          local fileicon = M.get_file_icon():match('%S+')
          for k, _ in pairs(icons) do
            if vim.fn.index(icons[k], icon) ~= -1 then return icon_colors[k] end
          end
        end

        table.insert(coms, {
          type = 'text',
          text = {
            M.get_file_icon(),
            hl = { fg = M.get_file_icon_color(), bg = '#e6fce6', sytle = 'italic' },
          },
        })
      end
    end
  end
  -- empty space in line
  table.insert(coms, {
    type = 'text',
    text = {
      ' ',
      hl = { fg = '#e6fce6', bg = '#e6fce6' },
    },
  })

  return coms
end

local opt = {
  show_at_least = 1,
}
if not vim.g.termux then
  opt = {
    show_at_least = 2,
  }
end
require('tabby').setup({
  components = components,
  opt = opt,
})
