local personnal = {
  normal = {
    a = { fg = '#ffffff', bg = '#986fec' },
    b = { fg = '#986fec', bg = '#fdf6e3' },
    c = { fg = '#000000', bg = '#fdf6e3' },
    x = { fg = '#000000', bg = '#fdf6e3' },
    y = { fg = '#986fec', bg = '#fdf6e3' },
    z = { fg = '#ffffff', bg = '#986fec' },
  },
  insert = {
    a = { fg = '#ffffff', bg = '#2bbb4f' },
    b = { fg = '#2bbb4f', bg = '#fdf6e3' },
    c = { fg = '#000000', bg = '#fdf6e3' },
    x = { fg = '#000000', bg = '#fdf6e3' },
    y = { fg = '#2bbb4f', bg = '#fdf6e3' },
    z = { fg = '#ffffff', bg = '#2bbb4f' },
  },
  visual = {
    a = { fg = '#ffffff', bg = '#4799eb' },
    b = { fg = '#4799eb', bg = '#fdf6e3' },
    c = { fg = '#000000', bg = '#fdf6e3' },
    x = { fg = '#000000', bg = '#fdf6e3' },
    y = { fg = '#4799eb', bg = '#fdf6e3' },
    z = { fg = '#ffffff', bg = '#4799eb' },
  },
  replace = {
    a = { fg = '#ffffff', bg = '#ff0000' },
    b = { fg = '#ff0000', bg = '#fdf6e3' },
    c = { fg = '#000000', bg = '#fdf6e3' },
    x = { fg = '#000000', bg = '#fdf6e3' },
    y = { fg = '#ff0000', bg = '#fdf6e3' },
    z = { fg = '#ffffff', bg = '#ff0000' },
  },
  command= {
    a = { fg = '#ffffff', bg = '#e27d60' },
    b = { fg = '#e27d60', bg = '#fdf6e3' },
    c = { fg = '#000000', bg = '#fdf6e3' },
    x = { fg = '#000000', bg = '#fdf6e3' },
    y = { fg = '#e27d60', bg = '#fdf6e3' },
    z = { fg = '#ffffff', bg = '#e27d60' },
  },
  inactive = {
    a = { fg = '#000000', bg = '#fdf6e3' },
    b = { fg = '#000000', bg = '#fdf6e3' },
    c = { fg = '#000000', bg = '#fdf6e3' },
    x = { fg = '#000000', bg = '#fdf6e3' },
    y = { fg = '#000000', bg = '#fdf6e3' },
    z = { fg = '#000000', bg = '#fdf6e3' },
  },
}

local function blank()
  return ' '
end

local lualine_a = {
  { blank, padding = 0, color = {fg = '#000000', bg= '#fdf6e3'} },
  { 'mode' },
  { blank, padding = 0, color = {fg = '#000000', bg= '#fdf6e3'} },
  { '%l/%L' },
  { blank, padding = 0, color = {fg = '#000000', bg= '#fdf6e3'} },
  { 'progress' },
  { blank, padding = 0, color = {fg = '#000000', bg= '#fdf6e3'} },
  {
    'fileformat',
    icons_enabled = true,
    symbols = {
      unix = ' UNIX', -- e712
      dos = ' DOS', -- e70f
      mac = ' MAC', -- e711
    }
  },
  { blank, padding = 0, color = {fg = '#000000', bg= '#fdf6e3'} },
  {
    'encoding',
    fmt = string.upper,
  },
  { blank, padding = 0, color = {fg = '#000000', bg= '#fdf6e3'} },
}

local lualine_b = {
  { '%S', padding = 0 },
}

local lualine_y = {
  {
    'filetype',
    colored = true, -- displays filetype icon in color if set to `true
    icon_only = false -- Display only icon for filetype
  },
}
local filename = {
  'filename',
  file_status = true,  -- displays file status (readonly status, modified status)
  path = 2,            -- 0 = just filename, 1 = relative path, 2 = absolute path
  shorting_target = 40 -- Shortens path to leave 40 space in the window
                       -- for other components. Terrible name any suggestions?
}
if not vim.g.termux then
  lualine_y = {
    filename,
    {
      'filetype',
      colored = true, -- displays filetype icon in color if set to `true
      icon_only = false -- Display only icon for filetype
    },
  }
end

local lualine_z = { '%r' }

require('lualine').setup {
  options = {
    theme = personnal,
    section_separators = '', component_separators = ''
  },
  sections = {
    lualine_a = lualine_a,
    lualine_b = lualine_b,
    lualine_c = {},
    lualine_x = {},
    lualine_y = lualine_y,
    lualine_z = lualine_z,
  },
  inactive_sections = {
    lualine_a = lualine_a,
    lualine_b = {},
    lualine_c = {},
    lualine_x = {},
    lualine_y = lualine_y,
    lualine_z = lualine_z,
  },
}
