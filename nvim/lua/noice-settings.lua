require("notify").setup({ top_down = false })
require("noice").setup({
  views = {
    cmdline_popup = {
      relative = "editor",
      position = {
        row = 3,
        col = "55%",
      },
      size = {
        width = "70%",
        height = 3,
      },
      win_options = {
        wrap = true,
      },
    },
    popupmenu = {
      relative = "editor",
      position = {
        row = 8,
        col = "55%",
      },
      size = {
        width = "70%",
        height = "60%",
      },
      border = {
        style = "rounded",
      },
    },
  },
  routes = {
    {
      view = "notify",
      filter = { event = "msg_showmode" },
    },
  },
})
