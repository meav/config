local actions = require "telescope.actions"

local transform_mod = require("telescope.actions.mt").transform_mod
local nvb_actions_1 = transform_mod {
  excute_file = function(prompt_bufnr)
    local selections = fb_utils.get_selected_files(prompt_bufnr, true)
    for _, selection in ipairs(selections) do
      require("plenary.job")
        :new({
          command = "explorer",
          args = { selection:absolute() },
        })
        :start()
    end
    actions.close(prompt_bufnr)
  end,
}

require('telescope').setup{
  defaults = {
    sorting_strategy = 'ascending',
    scroll_strategy = 'limit',
    layout_strategy = 'center',
    layout_config = {
      center = {
        height = 0.9,
        preview_cutoff = 200,
        prompt_position = "top",
        width = 0.8
      },
    },
    history = false,
    preview = false,
    mappings = {
      i = {
        ["<C-n>"] = actions.move_selection_next,
        ["<C-p>"] = actions.move_selection_previous,
      
        ["<C-c>"] = actions.close,
      
        ["<Down>"] = actions.move_selection_next,
        ["<Up>"] = actions.move_selection_previous,
        ["<Tab>"] = actions.move_selection_next,
        ["<S-Tab>"] = actions.move_selection_previous,
      
        ["<CR>"] = actions.select_default,
        ["<C-o>"] = actions.select_default,
        ["<C-x>"] = actions.select_horizontal,
        ["<C-v>"] = actions.select_vertical,
        ["<C-t>"] = actions.select_tab,
        ["<C-x>"] = nvb_actions_1.excute_file,
      
        ["<C-u>"] = actions.preview_scrolling_up,
        ["<C-d>"] = actions.preview_scrolling_down,
      
        ["<PageUp>"] = actions.results_scrolling_up,
        ["<PageDown>"] = actions.results_scrolling_down,
      
        ["<Space>"] = actions.toggle_selection + actions.move_selection_worse,
        ["<S-Space>"] = actions.toggle_selection + actions.move_selection_better,
        ["<C-q>"] = actions.send_to_qflist + actions.open_qflist,
        ["<M-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
        ["<C-l>"] = actions.complete_tag,
        ["<C-_>"] = actions.which_key, -- keys from pressing <C-/>
        ["<C-w>"] = { "<c-s-w>", type = "command" },
      },
      
      n = {
        ["<esc>"] = actions.close,
        ["<C-Space>"] = actions.close,
        ["<C-b>"] = actions.close,
        ["<CR>"] = actions.select_default,
        ["o"] = actions.select_default,
        ["<C-x>"] = actions.select_horizontal,
        ["<C-v>"] = actions.select_vertical,
        ["<C-t>"] = actions.select_tab,
        ["t"] = actions.select_tab,
        ["x"] = nvb_actions_1.excute_file,
      
        ["<Space>"] = actions.toggle_selection + actions.move_selection_worse,
        ["<S-Space>"] = actions.toggle_selection + actions.move_selection_better,
        ["<C-q>"] = actions.send_to_qflist + actions.open_qflist,
        ["<M-q>"] = actions.send_selected_to_qflist + actions.open_qflist,
      
        -- TODO: This would be weird if we switch the ordering.
        ["j"] = actions.move_selection_next,
        ["k"] = actions.move_selection_previous,
        ["H"] = actions.move_to_top,
        ["M"] = actions.move_to_middle,
        ["L"] = actions.move_to_bottom,
      
        ["<Down>"] = actions.move_selection_next,
        ["<Up>"] = actions.move_selection_previous,
        ["<Tab>"] = actions.move_selection_next,
        ["<S-Tab>"] = actions.move_selection_previous,
        ["gg"] = actions.move_to_top,
        ["G"] = actions.move_to_bottom,
      
        ["<C-u>"] = actions.preview_scrolling_up,
        ["<C-d>"] = actions.preview_scrolling_down,
      
        ["<PageUp>"] = actions.results_scrolling_up,
        ["<PageDown>"] = actions.results_scrolling_down,
      
        ["?"] = actions.which_key,
      }
    }
  },
  pickers = {
    buffers = {
      show_all_buffers = true,
      sort_lastused = true,
      mappings = {
        i = {
          ["<C-d>"] = actions.delete_buffer,
        },
        n = {
          ["d"] = actions.delete_buffer,
        }
      }
    }
  },
}

vim.api.nvim_create_user_command("TelescopeCommand", function() require("telescope.builtin").builtin({ include_extensions = true }) end, {})
