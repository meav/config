vim.filetype.add({
    extension = {
        m3u = "m3u",
        m3u8 = "m3u",
        caddy = "caddyfile",
    },
    filename = {
        ["playlisttempt"] = "m3u",
        ["vifmrc"] = "vim",
    },
})
