local opt = vim.opt
local g = vim.g
local api = vim.api
-- Disable build-in plugins
local disabled_built_ins = {
	"netrw",
	"netrwPlugin",
	"netrwSettings",
	"netrwFileHandlers",
	"gzip",
	"zip",
	"zipPlugin",
	"tar",
	"tarPlugin",
	"getscript",
	"getscriptPlugin",
	"vimball",
	"vimballPlugin",
	"2html_plugin",
	"logipat",
	"rrhelper",
	"spellfile_plugin",
}
--
for _, plugin in pairs(disabled_built_ins) do
	g["loaded_" .. plugin] = 1
end
-- Nvimqt shortcut
-- start /max "" "C:\Program Files\neovim-qt 0.2.18\bin\nvim-qt.exe" %HOMEDRIVE%%HOMEPATH%\Desktop
-- if vim.fn.has "win32" == 1 then
-- 	api.nvim_create_augroup("guioption", {clear = true})
-- 	api.nvim_create_autocmd( "UIEnter", {
-- 		group = "guioption",
-- 	  once = true,
-- 	  callback = function()
-- 			vim.cmd([[
-- 				GuiFont! VictorMono NF:h16
-- 				GuiLinespace 1
-- 				GuiPopupmenu 0
-- 				GuiScrollBar 0
-- 				GuiTabline 0
-- 			]])
-- 			end
-- 	  })
-- end
opt.mouse = 'a'
-- Colorscheme
opt.termguicolors = true
opt.background = 'light'
-- Plugins
require("plugins")
if not g.termux then
	-- Disable SHADA
	opt.shadafile = 'NONE'
end
-- Linenumber
opt.number = true
opt.relativenumber = true
if g.termux then
	opt.numberwidth = 2
else
	opt.numberwidth = 5
end
-- Encoding
opt.fileencodings = {'utf-8', 'ucs-bom', 'gb18030', 'gbk', 'gb2312', 'cp936'}
opt.encoding = 'utf-8'
if g.termux then
	-- Scroll before top or bottom
	opt.scrolloff = 5
else
	opt.scrolloff = 7
end
-- Key mapping
local function map(mode, lhs, rhs, opts)
	local options = {noremap = true, silent = true}
	if opts then
		options = vim.tbl_extend("force", options, opts)
	end
	api.nvim_set_keymap(mode, lhs, rhs, options)
end
map('', '<C-a>', [[<esc>:%y*<CR>]], opt)
map('', '<C-h>', [[<esc><C-w>h]], opt)
map('', '<C-Left>', [[<esc><C-w>h]], opt)
map('', '<C-j>', [[<esc><C-w>j]], opt)
map('', '<C-Down>', [[<esc><C-w>j]], opt)
map('', '<C-k>', [[<esc><C-w>k]], opt)
map('', '<C-Up>', [[<esc><C-w>k]], opt)
map('', '<C-l>', [[<esc><C-w>l]], opt)
map('', '<C-Right>', [[<esc><C-w>l]], opt)
map('i', '<C-Space>', [[<esc>]], opt)
map('v', '<C-Space>', [[<esc>]], opt)
map('n', '<C-Space>', [[<esc>]], opt)
map('c', '<C-Space>', [[<esc>]], opt)
map('t', '<C-Space>', [[<C-\><C-n>]], opt)
map('t', '<RightMouse><RightRelease>', [[<MiddleMouse>a]], opt)
-- Status and tabline
opt.showmode = false
opt.laststatus = 2
opt.showcmd = true
opt.showcmdloc = 'statusline'
-- Cursor
opt.cursorline = true
api.nvim_create_augroup("cursortheme", {clear = true})
api.nvim_create_autocmd( "ColorScheme", {
	group = "cursortheme",
  once = true,
  callback = function()
		vim.cmd([[
			hi Cursorgreen guifg=green guibg=green
			hi Cursorred guifg=red guibg=red
		]])
		end
  })
opt.guicursor = 'n-v-ve:hor20-Cursorgreen/lCursorgreen,i-c-ci:ver25-Cursorgreen/lCursorgreen,r-cr:hor20-blinkwait700-blinkon1000-blinkoff700-Cursorred/lCursorred,o:hor20-Cursorred/lCursorred'
-- Broot
vim.cmd([[
	noremap <C-B> :call RunBroot()<CR>i

	fun! RunBroot()
		if has('win32')
	  	let l:command = 'broot --conf %HOMEDRIVE%%HOMEPATH%\AppData\Roaming\dystroy\broot\config\conf.hjson;%HOMEDRIVE%%HOMEPATH%\AppData\Roaming\dystroy\broot\config\neovimbroot.hjson %HOMEDRIVE%%HOMEPATH%'
		else
			let l:command = "broot --conf '/data/data/com.termux/files/home/.config/broot/conf.hjson;/data/data/com.termux/files/home/.config/broot/neovimbroot.hjson'"
		endif
	  tabnew
	  call termopen(l:command, {'on_exit': 'BrootOnExit'})
	endfun

	fun! BrootOnExit(job_id, code, event) dict
	  let l:filename = getline(1) . getline(2) . getline(3) . getline(4) . getline(5) . getline(6) . getline(7) . getline(8) . getline(9) . getline(10)
	  enew | bd! #

	  if (l:filename != '')
	    execute 'edit ' . l:filename
	  else
	    bp
	  endif
	endfun
]])
-- Personal alias
if vim.g.termux then
	vim.api.nvim_create_user_command("E", function() vim.cmd [[call feedkeys("\<C-B>")]] end, {})
end
api.nvim_create_user_command('Broot', function() vim.cmd [[call feedkeys("\<C-B>")]] end, {})
if vim.fn.has "win32" == 1 then
	local home = os.getenv("USERPROFILE")
	api.nvim_create_user_command('OpenCurrentFile', function() vim.cmd('!explorer %') end, {})
	api.nvim_create_user_command('CMD', 'tabedit term://cmd', {})
	api.nvim_create_user_command('PWSH', 'tabedit term://pwsh', {})
	api.nvim_create_user_command('Power', function()
		os.execute('del ' .. home .. '\\AppData\\Roaming\\Microsoft\\Windows\\PowerShell\\PSReadLine\\ConsoleHost_history.txt')
		os.execute('del /S /Q ' .. home .. '\\AppData\\Roaming\\mpv\\gallery-thumbs-dir\\*')
		os.execute('explorer "D:\\Users\\Doanh\\Pictures\\Tắt máy.vbs"')
		vim.cmd('quitall!')
	end, {})
end
if not g.termux then
	-- Change directory to Desktop
	vim.cmd('cd ~')
end
