parser = clink.arg.new_parser

excludelocal = '--update --ignore-size --progress -v --exclude "/Chuong trinh moi/**" "D:\\Me Doanh\\TH Tu Liem" "vtd:TH Tu Liem"'
excludecloud = '--update --ignore-size --progress -v --exclude "/Chuong trinh moi/**" "vtd:TH Tu Liem" "D:\\Me Doanh\\TH Tu Liem"'
includelocal = '--update --ignore-size --progress -v --include "/Giao an Word/**" "D:\\Me Doanh\\TH Tu Liem\\Chuong trinh moi" "vtd:Chuong trinh moi"'
includecloud = '--update --ignore-size --progress -v --include "/Giao an Word/**" "vtd:Chuong trinh moi" "D:\\Me Doanh\\TH Tu Liem\\Chuong trinh moi"'

local goals = parser(
	{
		'sync' ..
			parser(
				{
				'--dry-run' .. 
					parser(
						{
						'--log-file=log' .. 
							parser(
								{
								excludelocal,
								excludecloud,
								includelocal,
								includecloud,
								'aa'
								}
							),
						excludelocal,
						excludecloud,
						includelocal,
						includecloud,
						'aa'

						}
					)
				}
			)
	}
):loop(1)

clink.arg.register_parser('rclone', goals)
